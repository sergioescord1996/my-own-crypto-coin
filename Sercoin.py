'''
Title: Sercoin with Python
Author: Sergio Escalante Ordóñez

'''

# MARK: Imports

import hashlib
import datetime
import json
import pprint
from time import time

# MARK: Block class

class Block:
    
    # MARK: Constructor
    
    def __init__(self, timestamp, trans, previousBlock = ''):
        self.timestamp = timestamp
        self.trans = trans
        self.previousBlock = previousBlock
        self.difficultyIncrement = 0
        self.hash = self.calculateHash(trans, timestamp, self.difficultyIncrement)
        
    # MARK: Calculate hash    
    
    def calculateHash(self, data, timestamp, difficultyIncrement):
        
        data = str(data) + str(timestamp) + str(difficultyIncrement)
        data = data.encode()
        hash = hashlib.sha256(data)
        
        return hash.hexdigest()
        
    # MARK: Mine block
    
    def mineBlock(self, difficulty):
        
        difficultyCheck = "0" * difficulty
        
        while self.hash[:difficulty] != difficultyCheck:
            self.hash = self.calculateHash(self.trans, self.timestamp, self.difficultyIncrement)
            self.difficultyIncrement += 1
            
# MARK: Class Blockchain

class Blockchain:
    
    # MARK: Constructor
    
    def __init__(self):
        self.chain = [self.GenesisBlock()]
        self.difficulty = 5
        self.pendingTransactions = []
        self.reward = 10
    
    # MARK: Create genesis block
    
    def GenesisBlock(self):
        genesisBlock = Block(str(datetime.datetime.now()), "First block in the blockchain")
        return genesisBlock
    
    # MARK: Get last block
    
    def getLastBlock(self):
        return self.chain[-1]
    
    # MARK: Mine pending transactions
    
    def minePendingTrans(self, minerRewardAddress):
        newBlock = Block(str(datetime.datetime.now()), self.pendingTransactions)
        newBlock.mineBlock(self.difficulty)
        newBlock.previousBlock = self.getLastBlock().hash
        
        print(f"Hash for previous block: {newBlock.previousBlock}")
        
        testChain = []
        for trans in newBlock.trans:
            temp = json.dumps(trans.__dict__, indent=5, separators=(',',':'))
            testChain.append(temp)
        pprint.pprint(testChain)
        
        self.chain.append(newBlock)
        print(f"Block hash: {newBlock.hash}")
        print("New block added!!")
        
        rewardTrans = Transaction("System", minerRewardAddress, self.reward)
        self.pendingTransactions.append(rewardTrans)
        self.pendingTransactions = []
        
    # MARK: Validate chain    
    
    def isChainValid(self):
        
        for x in range(1, len(self.chain)):
            currentBlock = self.chain[x]
            previousBlock = self.chain[x-1]
            
            if (currentBlock.previousBlock != previousBlock.hash):
                print("Chain is valid")
                
        print("Chain is valid and secure!")
    
    # MARK: Create transaction
    
    def createTrans(self, transaction):
        self.pendingTransactions.append(transaction)
    
    # MARK: Get balance 
    
    def getBalance(self, walletAddress):
        balance = 0
        for block in self.chain:
            if block.previousBlock == "":
                continue
            for transaction in block.trans:
                if transaction.fromWallet == walletAddress:
                    balance -= transaction.amount
                if transaction.toWallet == walletAddress:
                    balance += transaction.amount
        return balance
    
# MARK: Transaction class

class Transaction:
    
    # MARK: Constructor
    
    def __init__(self, fromWallet, toWallet, amount):
        self.fromWallet = fromWallet
        self.toWallet = toWallet
        self.amount = amount
        
        
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# ------------------------------------ TESTING ZONE -----------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------

blockchain = Blockchain()

print("Chemita is mining...")

blockchain.createTrans(Transaction("Macia", "JB", 0.01))
blockchain.createTrans(Transaction("Pepe", "Joan", 100))
blockchain.createTrans(Transaction("Chema", "Sergio", 0.5))
blockchain.createTrans(Transaction("Perez", "Antonio", 12))

startTime = time()
blockchain.minePendingTrans("Chemita")
endTime = time()

print(f"Chemita spent: {endTime - startTime}s")

print("-"*20)

print("Mar is mining...")

blockchain.createTrans(Transaction("Sergio", "Wiz khalifa", 0.1))
blockchain.createTrans(Transaction("Snoop Dog", "Eminem", 4))
blockchain.createTrans(Transaction("Elon", "Sergio", 500))

startTime = time()
blockchain.minePendingTrans("Mar")
endTime = time()

print(f"Colega spent: {endTime - startTime}s")

print("-"*20)

print("Colega is mining...")

blockchain.createTrans(Transaction("Jose", "John", 0.001))
blockchain.createTrans(Transaction("Juan", "Adolfo", 1.5))
blockchain.createTrans(Transaction("Smith", "Chen", 32))

startTime = time()
blockchain.minePendingTrans("Colega")
endTime = time()

print(f"Colega spent: {endTime - startTime}s")

print("-"*30)

print("Chemita has: " + str(blockchain.getBalance("Chemita")) + " Sercoins in his wallet")
print("Mar has: " + str(blockchain.getBalance("Mar")) + " Sercoins in her wallet")
print("Colega has: " + str(blockchain.getBalance("Colega")) + " Sercoins in his wallet")

for x in range(len(blockchain.chain)):
    print(f"Block hash {x}: {blockchain.chain[x].hash}")

print("-"*30)

print(blockchain.isChainValid())